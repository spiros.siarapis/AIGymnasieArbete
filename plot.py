import matplotlib.pyplot as plt
import json
import config


e = config.epochs
u = config.units

# Read the data from data.json
f = open("data.json", "r")

# and convert to json dictionary
jsonstr = f.read()
jsondict = json.loads(jsonstr)

accuracies = []
losses = []
titles = []

for i in range(e):  # Epochs
    epochs = config.IndexToEpoch(i)  # 3, 5, 7, 9, 11, 13
    plt.title("Epochs: {0}".format(epochs), loc="left")

    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")

    for j in range(u):  # Units
        units = config.IndexToUnit(j)  # 32, 64, 128, 256, 512, 1024

        title = "E: {0} | U: {1}".format(epochs, units)

        data = jsondict[title]
        plt.plot(range(1, epochs+1), data["history"], marker="o", label="Units: {0}".format(units))

        losses.append(data["evaluation"][0])
        accuracies.append(data["evaluation"][1])
        titles.append("E{0}U{1}".format(epochs, units))

    plt.legend(loc="lower right")
    plt.grid()
    plt.show()

roundPrec = 10**6

for i in range(e):
    minimum = i * u
    maximum = (i + 1) * u
    acc = [(round(x*roundPrec)/roundPrec) for x in accuracies[minimum:maximum]]

    plt.title("Accuracies {0}/6".format(i+1))
    plt.ylim([0.9, 1])

    plt.bar(titles[minimum:maximum], acc)
    plt.show()

for i in range(e):
    minimum = i * u
    maximum = (i + 1) * u
    loss = [(round(x*roundPrec)/roundPrec) for x in losses[minimum:maximum]]

    plt.title("Losses {0}/6".format(i+1))
    plt.ylim([0, 0.2])

    plt.bar(titles[minimum:maximum], loss)
    plt.show()
