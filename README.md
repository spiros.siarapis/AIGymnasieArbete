# AI GymnasieArbete
## Description
This is a school-project about image recognition and AI.
This project tries to find out the relationship between epochs and neurons when trying to make a tensorflow sequential model.

---

## Usage
The project contains some python scripts which you can run.  
The first script, "main.py", contains code for the creation and execution of the tensorflow model.
It also dumbs all the data that it gets to a separate json file called "data.json".  
The second script, "config.py", contains the functions necessary to convert from for-loop index to epochs and neurons respectively.  
The last script, "plot.py", when run creates matplotlib plots that plot out the data from the "data.json" file. 
