# Epochs to use from 3, for example: epochs=3; 3, 5, 7, 9, 11, 13
epochs = 6

# Units from 32 to use, for example: units=4; 32, 64, 128, 256, 512, 1024
units = 6


# Function that converts from for-loop index to epochs
def IndexToEpoch(index):
    return 2 * index + 3


# Function that converts from for-loop index to units
def IndexToUnit(index):
    return 2**(5+index)