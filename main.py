import tensorflow as tf
import json
import config

e = config.epochs
u = config.units

jsondict = {}

# Get the mnist dataset to use
mnist = tf.keras.datasets.mnist

# Divide the set between models
(x_train, y_train),(x_test, y_test) = mnist.load_data()
# Every pixel from 0 to 1 instead of 0 to 255
x_train, x_test = x_train / 255.0, x_test / 255.0

for i in range(e):  # Epochs
    for j in range(u):  # Units
        epochs = config.IndexToEpoch(i)  # 3, 5, 7, 9, 11, 13
        units = config.IndexToUnit(j)  # 32, 64, 128, 256, 512, 1024

        print()
        print("Epochs: {0} | Units: {1}".format(epochs, units))

        # Create a model with 3 layers,
        # First layer takes input in form of 28 * 28 pixels
        # Second layer is the hidden layer which has 'units' amount of neurons
        # Third layer outputs the final value which can be a number from 0 to 9
        model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape=(28, 28)),
            tf.keras.layers.Dense(units, activation="relu"),
            tf.keras.layers.Dense(10, activation='softmax')
        ])

        # Compile the model with the specific optimizer, loss function and metrics
        model.compile(optimizer='adam',
                      loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        # Train the model with the x_train data, y_train data and 'epochs' amount epochs
        history = model.fit(x_train, y_train, epochs=epochs)

        # Evaluate how good the model is, contains accuracy and loss values
        evaluation = model.evaluate(x_test, y_test)

        # Put all the data in a list of dictionaries
        jsondict["E: {0} | U: {1}".format(epochs, units)] = {
            "history": history.history["accuracy"],
            "evaluation": evaluation
        }

# Make the list of dictionaries into a json string
jsonstr = json.dumps(jsondict, indent=4)

print(jsonstr)

# Write the json string to the data.json file
f = open("data.json", "w")
f.write(jsonstr)
f.close()